package com.brom.epam.task06.model;

import com.brom.epam.task06.annotation.MyAnnotation;
import com.brom.epam.task06.view.View;
import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Model {
  private Logger logger = LogManager.getLogger(Model.class.getName());
  @MyAnnotation(title = "modelName")
  private String modelName;
  @MyAnnotation(title = "anotherField", value = 5)
  private String anotherField;
  private String fieldWithoutAnnotation;
  private Object unknownObject;

  public void voidMethod() {
    unknownObject = new View();
  }

  public String stringMethod(String str) {
    return str;
  }

  public int add(int a, int b) {
    return a + b;
  }

  public void myMethod(String a, int... args) {
    logger.info("Invoking myMethod(String, int[]) with params:" + a + Arrays.toString(args));
  }

  public void myMethod(String... args) {
    logger.info("Invoking myMethod(String[]) with params:" + Arrays.toString(args));
  }
}
