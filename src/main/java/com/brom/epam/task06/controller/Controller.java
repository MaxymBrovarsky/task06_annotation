package com.brom.epam.task06.controller;

import com.brom.epam.task06.annotation.MyAnnotation;
import com.brom.epam.task06.model.Model;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
  private Model model;
  private Logger logger = LogManager.getLogger(Controller.class.getName());

  public Controller() {
    this.model = new Model();
  }
  public List<Field> getFieldsThatWereAnnotated() {
    Class modelClass = model.getClass();
    Field[] fields = modelClass.getDeclaredFields();
    List<Field> fieldsList = Arrays.asList(fields).stream()
        .filter(f -> f.isAnnotationPresent(MyAnnotation.class))
        .collect(Collectors.toList());
    return fieldsList;
  }

  public void invokeMethods() {
    try {
      Class modelClass = model.getClass();
      logger.info("Invoking void voidMethod()");
      Method voidMethod = modelClass.getMethod("voidMethod");
      voidMethod.invoke(model);
      logger.info("Invoking String stringMethod(String str)");
      Method stringMethod = modelClass.getMethod("stringMethod", String.class);
      String result = (String) stringMethod.invoke(model, "testString");
      logger.info("Return value from stringMethod(testString) = " + result);
      Method addMethod = modelClass.getMethod("add", int.class, int.class);
      int sum = (int) addMethod.invoke(model, 1, 2);
      logger.info("Return value from add(1, 2) = " + sum);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      logger.error("something bad happened");
    }
  }

  public void setFieldValue() {
    try {
      Class modelClass = model.getClass();
      Field field = modelClass.getDeclaredField("modelName");
      field.setAccessible(true);
      logger.info("field type = " + field.getType());
      logger.info("setting field to value created with empty constructor");
      field.set(model, field.getType().getConstructor().newInstance());
      logger.info(field.get(model).hashCode());
    } catch (NoSuchFieldException | NoSuchMethodException e) {
      logger.error("something bad happened");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    }
  }

  public void invokingMethodsWithVarargsParameters() {
    try {
      Class modelClass = model.getClass();
      Method method1 = modelClass.getMethod("myMethod", String.class, int[].class);
      Method method2 = modelClass.getMethod("myMethod", String[].class);
      method1.invoke(model, "hello", new int[] {1, 2});
      String[] args = new String[]{"hello", "my", "friend"};
      method2.invoke(model, new Object[] {args});
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      logger.error("error");
    }
  }

  public void showInformationAboutUnknownClass() {
    try {
      Class modelClass = model.getClass();
      Class clazz = modelClass.getDeclaredField("unknownObject").getType().getClass();
      logger.info("---------------FIELDS--------------------------");
      Arrays.asList(clazz.getDeclaredFields()).forEach(f -> logger.info(f.toString()));
      logger.info("---------------CONSTRUCTORS--------------------------");
      Arrays.asList(clazz.getDeclaredConstructors()).forEach(c -> logger.info(c.toString()));
      logger.info("---------------ANNOTATIONS--------------------------");
      Arrays.asList(clazz.getDeclaredAnnotations()).forEach(a -> logger.info(a.toString()));
      logger.info("---------------METHODS--------------------------");
      Arrays.asList(clazz.getDeclaredMethods()).forEach(m -> logger.info(m.toString()));
    } catch (NoSuchFieldException e) {
      logger.error("error");
      e.printStackTrace();
    }
  }
}
