package com.brom.epam.task06.view;

import com.brom.epam.task06.annotation.MyAnnotation;
import com.brom.epam.task06.controller.Controller;
import java.lang.reflect.Field;
import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private Map<String, Command> menuCommands;
  private Map<String, String> menu;
  private Scanner input = new Scanner(System.in);
  private Logger logger = LogManager.getLogger(View.class.getName());
  private Controller controller;

  public View() {
    initMenu();
    initMenuCommands();
    this.controller = new Controller();
  }

  private void initMenu() {
    this.menu = new LinkedHashMap<>();
    this.menu.put("1", "1. Print fields that were annotated");
    this.menu.put("2", "2. Print annotation value");
    this.menu.put("3", "3. Invoke three different methods");
    this.menu.put("4", "4. Setting value to field of unknown type");
    this.menu.put("5", "5. Invoking methods with varargs");
    this.menu.put("6", "6. Show information about unknown object");
    this.menu.put("7", "7. Quit");
  }

  private void initMenuCommands() {
    this.menuCommands = new LinkedHashMap<>();
    this.menuCommands.put("1", this::printFieldsThatWereAnnotated);
    this.menuCommands.put("2", this::printAnnotationValue);
    this.menuCommands.put("3", this::printMethodsInvocationInfo);
    this.menuCommands.put("4", this::printFieldValueSettingInfo);
    this.menuCommands.put("5", this::invokingMethodsWithVarargs);
    this.menuCommands.put("6", this::printInfoAboutUnknownClass);
    this.menuCommands.put("7", this::quit);
  }

  private void printInfoAboutUnknownClass() {
    this.controller.showInformationAboutUnknownClass();
  }

  private void quit() {
    System.exit(0);
  }

  private void invokingMethodsWithVarargs() {
    this.controller.invokingMethodsWithVarargsParameters();
  }

  private void printFieldValueSettingInfo() {
    this.controller.setFieldValue();
  }

  private void printFieldsThatWereAnnotated() {
    List<Field> fields = this.controller.getFieldsThatWereAnnotated();
    logger.info("Fields that were annotated with MyAnnotation");
    fields.forEach(f -> logger.info(f.getName()));
  }

  private void printAnnotationValue() {
    this.printFieldsThatWereAnnotated();
    logger.info("Enter name of field for printing annotation value");
    String fieldName = input.nextLine();
    List<Field> annotatedFields = this.controller.getFieldsThatWereAnnotated();
    Optional<Field> fieldOptional =
        annotatedFields.stream().filter(f -> f.getName().equals(fieldName)).findFirst();
    if (fieldOptional.isPresent()) {
      MyAnnotation myAnnotation = fieldOptional.get().getAnnotation(MyAnnotation.class);
      logger.info("title = " + myAnnotation.title());
      logger.info("value = " + myAnnotation.value());
    } else {
      logger.error("Here no such field");
    }
  }

  private void printMethodsInvocationInfo() {
    this.controller.invokeMethods();
  }

  public void show() {
    while (true) {
      try {
        this.menu.values().forEach(v -> logger.info(v));
        String command = input.nextLine();
        this.menuCommands.get(command).execute();
      } catch (InputMismatchException e) {
        logger.error("Please enter correct command");
      }
    }
  }

}
