package com.brom.epam.task06.view;

public interface Command {
  void execute();
}
