package com.brom.epam.task06;

import com.brom.epam.task06.view.View;

public class Application {
  public static void main(String[] args) {
    new View().show();
  }
}
